import net.ltgt.gradle.errorprone.errorprone
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    `java-library`
    `maven-publish`
    idea
    eclipse
    checkstyle
    id("net.ltgt.errorprone").version("1.1.1")
}

group = "com.xenoterracide"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}
val sourcesJar by tasks.registering(Jar::class) {
    dependsOn(JavaPlugin.CLASSES_TASK_NAME)
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

val javadocJar by tasks.registering(Jar::class) {
    archiveClassifier.set("javadoc")
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    from(tasks.getByPath("javadoc"))
}

artifacts {
    archives(javadocJar.get())
    archives(sourcesJar.get())
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<JavaCompile>().configureEach {
    options.compilerArgs.addAll(listOf("-parameters"))
    options.errorprone {
        error("Var")
        option("NullAway:AnnotatedPackages", "com.potreromed")
    }

}

tasks.getByPath("checkstyleTest").enabled = false

tasks.withType<Checkstyle>().configureEach {
    reports {
        xml.isEnabled = false
        html.isEnabled = false
    }
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
    testLogging { info { events.add(TestLogEvent.PASSED) } }
    reports {
        junitXml.isEnabled = true
        html.isEnabled = false
    }
}

dependencies {
    annotationProcessor(enforcedPlatform("org.springframework.boot:spring-boot-starter-parent:2.2.+"))
    implementation(enforcedPlatform("org.springframework.boot:spring-boot-starter-parent:2.2.+"))

    annotationProcessor("org.projectlombok:lombok:1.+")
    compileOnly("org.projectlombok:lombok:1.+")

    annotationProcessor( "com.uber.nullaway:nullaway:0.7.+")
    errorprone("com.google.errorprone:error_prone_core:2.+")
    compileOnly("com.google.errorprone:error_prone_annotations:2.+")

    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    implementation("org.springframework:spring-tx")
    implementation("org.springframework:spring-context")
    implementation("org.springframework.security:spring-security-core")
    implementation("org.springframework.security:spring-security-crypto")
    implementation("org.springframework.boot:spring-boot-autoconfigure")
    runtimeOnly("org.springframework.boot:spring-boot-starter-logging")
    implementation("org.apache.httpcomponents:fluent-hc")
    implementation("org.apache.httpcomponents:httpclient")
    implementation("com.fasterxml.jackson.core:jackson-annotations")
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("org.apache.logging.log4j:log4j-api")

    testRuntimeOnly("org.postgresql:postgresql")
    testImplementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    testImplementation("org.springframework:spring-web")

    testImplementation("org.springframework:spring-test")
    testImplementation("org.springframework.boot:spring-boot-test")
    testImplementation("org.springframework.boot:spring-boot-test-autoconfigure")
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.mockito:mockito-core")
    testImplementation("nl.jqno.equalsverifier:equalsverifier:3.+")
    testImplementation("org.awaitility:awaitility:3.1.+")
    testImplementation("org.eclipse.paho:org.eclipse.paho.client.mqttv3:1.2.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}
