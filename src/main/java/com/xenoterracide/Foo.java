package com.xenoterracide;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Foo {
    private final String value;
}
