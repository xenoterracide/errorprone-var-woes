package com.xenoterracide;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MyService {
    private final String baz;

    @Transactional
    public Optional<String> save( Foo foo ) {
        var bar = foo.getValue() == null ? baz : foo.getValue();

        return Optional.ofNullable( bar );
    }
}
